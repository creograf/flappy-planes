
function MainMenu() {

  this.music = null;
  this.playButton = null;

}

MainMenu.prototype = {

  create: function () {

    this.playButton = this.add.button(400, 600, 'playButton', this.startGame, this, 'buttonOver', 'buttonOut', 'buttonOver');

  },

  update: function () {
    //  Do some nice funky main menu effect here
  },

  startGame: function () {
    this.state.start('Game');
  }

};

module.exports = MainMenu;
