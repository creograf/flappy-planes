function Game() {
  // When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

  this.game;        //  a reference to the currently running game
  this.add;     //  used to add sprites, text, groups, etc
  this.camera;  //  a reference to the game camera
  this.cache;       //  the game cache
  this.input;       //  the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
  this.load;        //  for preloading assets
  this.math;        //  lots of useful common math operations
  this.sound;       //  the sound manager - add a sound, play one, set-up markers, etc
  this.stage;       //  the game stage
  this.time;        //  the clock
  this.tweens;  // the tween manager
  this.world;       // the game world
  this.particles;   // the particle manager
  this.physics; // the physics manager
  this.rnd;     // the repeatable random number generator

  // You can use any of these from any function within this State.
  // But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.

  this.map = [1,1,1,0,1];
}

Game.prototype = {
  create: function () {
    var game = this;

    game.physics.startSystem(Phaser.Physics.ARCADE);

    game.add.sprite(0, 125, 'bg');

    game.houses = game.add.group();
    game.houses.enableBody = true;

    game.map.forEach(function(block, i) {
      if (!block) { return }

      game.housesLine = game.houses.create(200 * i, game.world.height - 200, 'houses');
      game.housesLine.immovable = true;
    });

  },

  update: function () {
  },

  quitGame: function () {
    // Here you should destroy anything you no longer need.
    // Stop music, delete sprites, purge caches, free resources, all that good stuff.

    // Then let's go back to the main menu.
    this.state.start('MainMenu');

  }
};

module.exports = Game;
