var io = require('socket.io-client');
global.PIXI = require('../lib/pixi');
global.Phaser = require('../lib/phaser');

var socket = io();

socket.on('connection', function() {
  console.log('connected');
});

var game = new Phaser.Game(1024, 768, Phaser.AUTO, 'game');

game.state.add('Boot', require('./states/boot'));
game.state.add('Preloader', require('./states/preloader'));
game.state.add('MainMenu', require('./states/main-menu'));
game.state.add('Game', require('./states/game'));

game.state.start('Boot');
