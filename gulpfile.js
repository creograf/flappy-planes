var path = require('path');
var gulp = require('gulp');
var eslint = require('gulp-eslint');
var uglify = require('gulp-uglify');
var del = require('del');
var Server = require('karma').Server;
var webpack = require('webpack-stream');
var nodemon = require('gulp-nodemon');

gulp.task("default", ['start', 'pack:dev', 'watch']);

gulp.task('build', ['buildjs']);

gulp.task('clean', function(next) {
    del(['./public/js/**'], next);
});

gulp.task('pack:dev', function() {
  return gulp.src('client/index.js')
    .pipe(webpack({
      watch: true,
      devtool: 'source-map',
      output: {
        filename: 'index.js'
      },
      resolve: {
        alias: {
          pixi: path.join(__dirname, "lib/pixi.js"),
          phaser: path.join(__dirname, "lib/phaser.js")
        }
      },
      module: {
        loaders: [{
          test: /phaser\.js$/, include: path.join(__dirname, 'lib'),
          loader: 'imports?PIXI=pixi'
        }]
      }
    }))
    .pipe(gulp.dest('public/js'));
});

gulp.task('pack:production', function() {
  return gulp.src('client/index.js')
    .pipe(webpack({
      output: {
        filename: 'index.js'
      },
      resolve: {
        alias: {
          pixi: path.join(__dirname, "lib/pixi.js"),
          phaser: path.join(__dirname, "lib/phaser.js")
        }
      },
      module: {
        loaders: [{
          test: /phaser\.js$/, include: path.join(__dirname, 'lib'),
          loader: 'imports?PIXI=pixi'
        }]
      }
    }))
    .pipe(gulp.dest('public/js'));
});

gulp.task('lint', function () {
  return gulp.src('./client/**/*.js')
    .pipe(eslint({
      rules: {
        'no-console': 0
      },
      globals: {
        Phaser: true
      },
      extends: 'eslint:recommended',
      envs: ['browser', 'commonjs']
    }))
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('start', function () {
  nodemon({
    script: 'server/index.js',
    ext: 'js html',
    cwd: __dirname,
    ignore: [],
    watch: ['server'],
    env: {
      'NODE_ENV': 'development'
    }
  })
})

gulp.task('connect', function() {
  connect.server({
    root: 'src',
    port: 3000,
    livereload: true
  });
});

gulp.task('test', function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});

gulp.task('watch', function () {
  gulp.watch(['./client/**/*'], ['lint']);
});
