var path = require('path')
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var publicDir = path.resolve(__dirname, '../public');
app.use(express.static(publicDir));

// app.get '/', (req, res) ->
//   res.sendFile()

var players = {};

function Player(socket) {
  self = this;

  self.coords = {
    x: 0,
    y: 0
  };

  self.moveRight = true;

  self.sendInterval = setInterval(function() {
    if (self.moveRight) {
      self.coords.x += 1;
    } else {
      self.coords.x -= 1;
    }

    if (self.coords.x >= 50 || self.coords.x <= -1) {
      self.moveRight = !self.moveRight;
    }

    socket.emit('coords', self.coords);
  }, 100);

  self.deactivate = function() {
    clearInterval(self.sendInterval);
  };
}

io.on('connection', function(socket){
  players[socket.id] = new Player(socket);

  socket.on('disconnect', function(){
    players[socket.id].deactivate();
    delete players[socket.id];
  });
});

http.listen(3000, function() {
  console.log('listening on *:3000');
});
